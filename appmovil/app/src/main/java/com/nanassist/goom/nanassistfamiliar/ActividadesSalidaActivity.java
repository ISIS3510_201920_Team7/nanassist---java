package com.nanassist.goom.nanassistfamiliar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;

public class ActividadesSalidaActivity extends NavegacionActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_actividades_salida, null, false);
        drawer.addView(contentView, 0);


        Intent intent = getIntent();

        final String correo = intent.getStringExtra("correo");
        final CardView CitasMed = (CardView) findViewById(R.id.imgCitasMed);
        final CardView Entretenimiento =(CardView) findViewById(R.id.imgEntretenimiento);
        final CardView Diligencias = (CardView)findViewById(R.id.imgDiligencias);
        final CardView Eventos = (CardView)findViewById(R.id.imgEventos);

        CitasMed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent CategoriasIntent = new Intent(ActividadesSalidaActivity.this, SalirActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("categoria","acompañamiento");
                ActividadesSalidaActivity.this.startActivity(CategoriasIntent);
            }
        });

        Entretenimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent CategoriasIntent = new Intent(ActividadesSalidaActivity.this,SalirActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("categoria","entretenimiento");
                ActividadesSalidaActivity.this.startActivity(CategoriasIntent);
            }
        });

        Diligencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent CategoriasIntent = new Intent(ActividadesSalidaActivity.this, SalirActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("categoria","diligencias");
                ActividadesSalidaActivity.this.startActivity(CategoriasIntent);
            }
        });

        Eventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent CategoriasIntent = new Intent(ActividadesSalidaActivity.this, SalirActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("categoria","movilidad");
                ActividadesSalidaActivity.this.startActivity(CategoriasIntent);
            }
        });
    }
}
