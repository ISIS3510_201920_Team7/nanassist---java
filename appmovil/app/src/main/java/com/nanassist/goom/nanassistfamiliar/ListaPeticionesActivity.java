package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.nanassist.goom.nanassistfamiliar.Model.PeticionesDentroDeCasa;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesFueraDeCasa;

import java.util.ArrayList;

public class ListaPeticionesActivity extends AppCompatActivity {

    ListView listaEstados;
    ArrayList<PeticionesDentroDeCasa> lsalidas;
    ArrayList<String> slista;
    ArrayList<PeticionesFueraDeCasa> lcasa;
    String correo;
    private BottomNavigationView bottomNav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estados);

        bottomNav = findViewById(R.id.bottomBar);
        bottomNav.setSelectedItemId(R.id.tab_services);
        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                switch (item.getItemId()) {
                    case R.id.tab_home:
                        Intent intent = new Intent(ListaPeticionesActivity.this,PrincipalActivity.class);
                        ListaPeticionesActivity.this.startActivity(intent);
                        //finish();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        break;
                    case R.id.tab_profile:
                        Intent intent2 = new Intent(ListaPeticionesActivity.this,PerfilActivity.class);
                        ListaPeticionesActivity.this.startActivity(intent2);
                        intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        //finish();
                        break;
                    case R.id.tab_services:

                        break;
                }


                return true;
            }
        });

        listaEstados= findViewById(R.id.lvEstados);
        lsalidas = (ArrayList<PeticionesDentroDeCasa>) getIntent().getSerializableExtra("salidas");
        lcasa = (ArrayList<PeticionesFueraDeCasa>) getIntent().getSerializableExtra("casa");
        slista = new ArrayList<String>();
        for(PeticionesDentroDeCasa p: lsalidas)
        {
            slista.add("\n"+"Categoria: " +p.getCategoria()+"\n" +"fecha: "+p.getFecha()+"\n");
        }
        for(PeticionesFueraDeCasa p: lcasa)
        {
            slista.add("\n"+"Categoria: " +p.getCategoria()+"\n" +"fecha: "+p.getFecha()+"\n");
        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,slista);
        listaEstados.setAdapter(adapter);
        listaEstados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position<=lsalidas.size()-1)
                {
                    PeticionesDentroDeCasa a=lsalidas.get(position);
                    String direccion1=a.getDireccion1();
                    String fecha=a.getFecha();
                    String hora=a.getHora();
                    String categoria=a.getCategoria();
                    String descripcion=a.getDescripcion();
                    String estado = a.getEstado();
                    String identificador = a.getIdentificador();
                    String asistente = a.getAsistente();

                    Intent intent = new Intent(ListaPeticionesActivity.this,DetalleActivity.class);

                    intent.putExtra("dir1",direccion1);
                    intent.putExtra("fecha",fecha);
                    intent.putExtra("hora",hora);
                    intent.putExtra("cat",categoria);
                    intent.putExtra("des",descripcion);
                    intent.putExtra("estado",estado);
                    intent.putExtra("identificador",identificador);
                    intent.putExtra("dentrodecasa",a);
                    intent.putExtra("asistente",asistente);
                    ListaPeticionesActivity.this.startActivity(intent);
                }
                else
                {
                    PeticionesFueraDeCasa a=lcasa.get(position-lsalidas.size());
                    String direccion1=a.getDireccion1();
                    String direccion2 = a.getDireccion2();
                    String fecha=a.getFecha();
                    String hora=a.getHora();
                    String categoria=a.getCategoria();
                    String descripcion=a.getDescripcion();
                    String estado = a.getEstado();
                    String identificador = a.getIdentificador();
                    String asistente = a.getAsistente();
                    Intent intent = new Intent(ListaPeticionesActivity.this,DetalleActivity.class);

                    intent.putExtra("dir1",direccion1);
                    intent.putExtra("dir2",direccion2);
                    intent.putExtra("fecha",fecha);
                    intent.putExtra("hora",hora);
                    intent.putExtra("cat",categoria);
                    intent.putExtra("des",descripcion);
                    intent.putExtra("estado",estado);
                    intent.putExtra("identificador",identificador);
                    intent.putExtra("asistente",asistente);
                    intent.putExtra("fueradecasa",a);
                    ListaPeticionesActivity.this.startActivity(intent);
                }



            }
        });
    }

   @Override
   public void onRestart() {
       super.onRestart();
       finish();
       startActivity(getIntent());
   }
}