package com.nanassist.goom.nanassistfamiliar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nanassist.goom.nanassistfamiliar.*;


public class LoginActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase db;
    DatabaseReference users;
    RelativeLayout loginLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText etName= (EditText) findViewById(R.id.etName);
        final EditText etPassword= (EditText) findViewById(R.id.etPassword);
        final Button bLogin = (Button) findViewById(R.id.bLogin);
        final TextView registerLink = (TextView) findViewById(R.id.tvRegisterHere);

        loginLayout= (RelativeLayout) findViewById(R.id.loginLayout);

        //Firebase Inicializacion
        auth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
        users = db.getReference("Clientes");



        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                LoginActivity.this.startActivity(registerIntent);
            }
        });

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = etName.getText().toString();
                final String password = etPassword.getText().toString();
                verificacionDatos();

            }
        });
    }

    private void verificacionDatos(){

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.e("WIFI","TIENE CONEXIÓN");
        } else {

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
            View mView = getLayoutInflater().inflate(R.layout.noconnection, null);
            mBuilder.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    //dialog.dismiss();
                }
            });
            mBuilder.setView(mView);
            mBuilder.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                }
            }, 5000);
            return;

        }
        final EditText etName= (EditText) findViewById(R.id.etName);
        final EditText etPassword= (EditText) findViewById(R.id.etPassword);

        boolean ok=true;
         if(TextUtils.isEmpty(etName.getText().toString())){
             Snackbar.make(loginLayout, "Por favor ingrese un Nombre ", Snackbar.LENGTH_SHORT).show();
             ok=ok&&false;
         }
        if(TextUtils.isEmpty(etPassword.getText().toString())){
            Snackbar.make(loginLayout, "Por favor ingrese una contraseña ", Snackbar.LENGTH_SHORT).show();
            ok=ok&&false;
        }

        if(ok){
            auth.signInWithEmailAndPassword(etName.getText().toString(), etPassword.getText().toString())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = auth.getCurrentUser();

                                String correo= etName.getText().toString();
                                String direccion= etPassword.getText().toString();
                                Intent intent = new Intent(LoginActivity.this, PrincipalActivity.class);
                                intent.putExtra("correo", correo);
                                intent.putExtra("direccion",direccion);
                                intent.putExtra("esctibir",true);
                                eliminarInformacionDePerfil();
                                LoginActivity.this.startActivity(intent);
                                finish();

                            } else {
                               AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setMessage("¡Ups! \n Correo y/o contraseña incorrecta").setPositiveButton("Reintentar", null).create().show();
                            }

                        }
                    });
        }
    }
    public void eliminarInformacionDePerfil() {
        SharedPreferences preferences = getSharedPreferences("datosperfil",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear().commit();

    }


    }

