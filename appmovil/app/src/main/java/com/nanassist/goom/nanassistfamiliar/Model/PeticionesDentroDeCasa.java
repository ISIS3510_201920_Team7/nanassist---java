package com.nanassist.goom.nanassistfamiliar.Model;

import java.io.Serializable;

public class PeticionesDentroDeCasa implements Serializable {
    String categoria, correo, descripcion, direccion1, fecha, hora, usuario,estado, identificador, asistente;


    public PeticionesDentroDeCasa(){}

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setDireccion(String direccion) {
        this.direccion1 = direccion;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getCorreo() {
        return correo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDireccion1() {
        return direccion1;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getUsuario() {
        return usuario;
    }
    public String getEstado() {
        return estado;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



    public void setAsistente(String idAsistente) {
        this.asistente = idAsistente;
    }

    public String getAsistente() {
        return asistente;
    }
}
