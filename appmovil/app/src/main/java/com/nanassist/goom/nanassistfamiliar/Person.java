package com.nanassist.goom.nanassistfamiliar;
public class Person {

    private String name;

    public Person() {
        // Constructor required for Firebase Database
    }

    public String getName() {
        return name;
    }

}