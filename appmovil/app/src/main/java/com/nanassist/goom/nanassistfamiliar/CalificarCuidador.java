package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CalificarCuidador extends AppCompatActivity {

    RatingBar rb;
    TextView value;
    Button calificar;
    String correo, correofamiliar, identificador;
    float calificacion;
    float calificacionActual;
    int numcalificaciones;
    RequestQueue queue;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedIntanceState) {
        super.onCreate(savedIntanceState);
        setContentView(R.layout.activity_calificar);

        db= FirebaseFirestore.getInstance();
        Intent i = getIntent();
        identificador = i.getStringExtra("identificador");
        rb = (RatingBar) findViewById(R.id.ratingBar);
        value = (TextView) findViewById(R.id.value);
        calificar = (Button) findViewById(R.id.button);
        correo = i.getStringExtra("correo");
        correofamiliar = i.getStringExtra("correofamiliar");
        calificacionActual= Float.valueOf(i.getStringExtra("calificacion"));
        numcalificaciones = Integer.valueOf(i.getStringExtra("numcalificaciones"));

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                value.setText("El valor es " + rating);
                calificacion = rating;
            }
        });

        calificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vaya = new Intent(CalificarCuidador.this, PrincipalActivity.class);
                float calificacion_final = (calificacionActual*numcalificaciones+calificacion)/(numcalificaciones+1);
                Map<String, Object> city = new HashMap<>();
                city.put("calificacion", calificacion_final);
                city.put("numcalificaciones", numcalificaciones+1);

                db.collection("Calificaciones").document(identificador)
                        .set(city)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("", "DocumentSnapshot successfully written!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("", "Error writing document", e);
                            }
                        });
                CalificarCuidador.this.startActivity(vaya);
                finish();
            }
        });
    }
}
