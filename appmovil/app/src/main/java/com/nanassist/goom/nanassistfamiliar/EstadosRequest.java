package com.nanassist.goom.nanassistfamiliar;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


public class EstadosRequest extends StringRequest{

    private static final String LOGIN_REQUEST_URL = "http://" + ipconstante.ip + "/Estados.php";
    private Map<String, String> params;

    public EstadosRequest(String correo, Response.Listener<String> listener){


        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);

        params = new HashMap<>();
        params.put("correo", correo);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
