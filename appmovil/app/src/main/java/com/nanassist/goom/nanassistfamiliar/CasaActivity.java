package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CasaActivity extends AppCompatActivity {

    private int dia, mes, anio, hora, minutos;

    TextView txtDescripcion;
    String correo, direccion, categoriaC;
    EditText etFecha, etHora, etDescripcion, etComentario;
    Button btAgendarServicio, btCancelar;
    String fecha;
    String sHora;
    String descripcion, auxDes, categoria, desc;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    CollectionReference peticionesDentroDeCasa;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casa);

        /*
         * Referencias para la conexion con firebase
         */
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        peticionesDentroDeCasa = db.collection("Peticiones");

        txtDescripcion = (TextView) findViewById(R.id.etDescripcion);
        if (getIntent() != null) {
            String info = getIntent().getStringExtra("descripcion");
            txtDescripcion.setText(info);
        }

        etFecha = findViewById(R.id.etFecha);
        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date = new Date();
                String fecha = dateFormat.format(date);
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                anio = c.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(CasaActivity.this, R.style.DialogFechaTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (monthOfYear < 10 && dayOfMonth < 10)
                            etFecha.setText("0" + dayOfMonth + "/" + "0" + monthOfYear + "/" + year);
                        else if (monthOfYear < 10) {
                            etFecha.setText(dayOfMonth + "/" + "0" + monthOfYear + "/" + year);
                        } else if (dayOfMonth < 10) {
                            etFecha.setText("0" + dayOfMonth + "/" + monthOfYear + "/" + year);
                        } else {
                            etFecha.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                        }
                    }
                }
                        , Integer.valueOf(fecha.split("-")[0]), Integer.valueOf(fecha.split("-")[1]) - 1, Integer.valueOf(fecha.split("-")[2]));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });


        etHora = findViewById(R.id.etHora);
        etHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                hora = c.get(Calendar.HOUR_OF_DAY);
                minutos = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(CasaActivity.this, R.style.DialogFechaTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay < 10 && minute < 10) {
                            etHora.setText("0" + hourOfDay + ":" + "0" + minute);
                        } else if (hourOfDay < 10) {
                            etHora.setText("0" + hourOfDay + ":" + minute);
                        } else if (minute < 10) {
                            etHora.setText(hourOfDay + ":" + "0" + minute);
                        } else {
                            etHora.setText(hourOfDay + ":" + minute);
                        }
                    }
                }, hora, minutos, false);
                timePickerDialog.show();
            }
        });


        etDescripcion = findViewById(R.id.etDescripcion);
        correo = auth.getCurrentUser().getEmail();
        direccion = "No se encontró dirección";
        DocumentReference docRef = db.collection("Clientes").document(auth.getCurrentUser().getUid());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d("", "DocumentSnapshot data: " + document.getData().get("address"));
                        direccion = (String) document.getData().get("address");
                    } else {
                        Log.d("", "No such document");
                    }
                } else {
                    Log.d("", "get failed with ", task.getException());
                }
            }
        });

        categoriaC = getIntent().getStringExtra("categoriaC");
        btAgendarServicio = findViewById(R.id.bAgendarServicio);
        btCancelar = findViewById(R.id.bCancelar);

        btAgendarServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fecha = etFecha.getText().toString();
                sHora = etHora.getText().toString();
                descripcion = etDescripcion.getText().toString();
                desc = descripcion;
                categoria = "Labores";
                auxDes = categoriaC;
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    Log.e("WIFI","TIENE CONEXIÓN");
                    AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(CasaActivity.this);
                    View mView2 = getLayoutInflater().inflate(R.layout.success, null);
                    mBuilder2.setView(mView2);
                    mBuilder2.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            guardarPeticion();
                            Intent intent = new Intent(CasaActivity.this, PrincipalActivity.class);
                            CasaActivity.this.startActivity(intent);
                            finish();
                        }
                    }, 1500);
                } else {

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(CasaActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.noconnection2, null);
                    mBuilder.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(CasaActivity.this);
                            View mView2 = getLayoutInflater().inflate(R.layout.success, null);
                            mBuilder2.setView(mView2);
                            mBuilder2.show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    Intent intent = new Intent(CasaActivity.this, PrincipalActivity.class);
                                    CasaActivity.this.startActivity(intent);
                                    finish();
                                }
                            }, 1500);

                        }
                    });
                    mBuilder.setView(mView);
                    mBuilder.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            guardarPeticion();

                        }
                    }, 1000);


                }

            }
        });

        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();

            }
        });
    }


    /*
    * Se almacena el nuevo registro en la base de datos firebase
    */
    public void guardarPeticion() {

        Map<String, Object> d = new HashMap<>();
        d.put("direccion1", direccion);
        d.put("fecha", fecha);
        d.put("hora", sHora);
        d.put("correo", correo);
        d.put("categoria", auxDes);
        d.put("descripcion", desc);
        d.put("usuario", auth.getCurrentUser().getUid() + "");
        d.put("estado", "disponible");
        d.put("asistente","Sin asignar");
        peticionesDentroDeCasa.add(d);


    }
    public void borrarStack()
    {
        Intent i = new Intent(this,CasaActivity.class);

    }
}
