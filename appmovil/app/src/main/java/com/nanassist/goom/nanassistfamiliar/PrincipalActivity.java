package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesDentroDeCasa;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesFueraDeCasa;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;

public class PrincipalActivity extends  NavegacionActivity{

    private FirebaseFirestore db;
    ArrayList<PeticionesDentroDeCasa> lsalidas;
    ArrayList<PeticionesFueraDeCasa> lcasa;
    private FirebaseAuth auth;
    private  AlertDialog.Builder mBuilder;
    private Dialog alerta;
    private boolean dentroCasa, fueraCasa;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        lsalidas = new ArrayList<>();
        dentroCasa =false;
        fueraCasa=false;


        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_options, null, false);
        drawer.addView(contentView, 0);



       Intent intent = getIntent();
        final String correo = intent.getStringExtra("correo");
        final String direccion = intent.getStringExtra("direccion");

        final ImageButton casa = findViewById(R.id.ibCasa);
        final ImageButton salir = findViewById(R.id.ibSalir);
        //final Button estado = findViewById(R.id.btnEstado);





        casa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent casaIntent = new Intent(PrincipalActivity.this, ActividadesCasaActivity.class);
                casaIntent.putExtra("correo",correo);
                casaIntent.putExtra("direccion",direccion);

                PrincipalActivity.this.startActivity(casaIntent);

            }
        });

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent salirIntent = new Intent(PrincipalActivity.this, ActividadesSalidaActivity.class);
                salirIntent.putExtra("correo",correo);
                PrincipalActivity.this.startActivity(salirIntent);

            }
        });


    }




    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }
}
