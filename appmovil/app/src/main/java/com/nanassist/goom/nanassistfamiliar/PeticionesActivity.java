package com.nanassist.goom.nanassistfamiliar;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesDentroDeCasa;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesFueraDeCasa;

import java.util.Map;


public class PeticionesActivity extends  NavegacionActivity {
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference notebookRef = db.collection("Peticiones");
    private FirebaseAuth auth;

    private SalidaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        auth = FirebaseAuth.getInstance();
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_peticiones, null, false);
        drawer.addView(contentView, 0);

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {

        Log.e("correo",auth.getCurrentUser().getEmail());
        Query query = notebookRef.whereEqualTo("correo", auth.getCurrentUser().getEmail());

        FirestoreRecyclerOptions<PeticionesFueraDeCasa> options = new FirestoreRecyclerOptions.Builder<PeticionesFueraDeCasa>()
                .setQuery(query, PeticionesFueraDeCasa.class)
                .build();

        adapter = new SalidaAdapter(options);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new SalidaAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                PeticionesDentroDeCasa note = documentSnapshot.toObject(PeticionesDentroDeCasa.class);
                String id = documentSnapshot.getId();
                String path = documentSnapshot.getReference().getPath();
                //Toast.makeText(PeticionesActivity.this,
                     //   "Position: " + position + " ID: " + id+ "fecha"+ documentSnapshot.getData().get("fecha"), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(PeticionesActivity.this, DetalleActivity.class);
                Map<String,Object> a= documentSnapshot.getData();
                String direccion1=a.get("direccion1").toString();
                String fecha=a.get("fecha").toString();
                String hora=a.get("hora").toString();
                String categoria=a.get("categoria").toString();
                String descripcion=a.get("descripcion").toString();
                String estado = a.get("estado").toString();;
//                String identificador = a.get("identificador").toString();;
                String asistente = a.get("asistente").toString();


                intent.putExtra("dir1",direccion1);
                if(a.get("direccion2")!=null)
                {
                    intent.putExtra("dir2",a.get("direccion2").toString());
                }
                intent.putExtra("fecha",fecha);
                intent.putExtra("hora",hora);
                intent.putExtra("cat",categoria);
                intent.putExtra("des",descripcion);
                intent.putExtra("estado",estado);
                //intent.putExtra("identificador",identificador);
                //intent.putExtra("dentrodecasa",a);
                intent.putExtra("asistente",asistente);
                PeticionesActivity.this.startActivity(intent);


            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}


