package com.nanassist.goom.nanassistfamiliar;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;
import java.util.Map;

public class InfoCuidadorRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "http://"+ ipconstante.ip+"/InfoCuidador.php";
    private Map<String, String> params;

    public InfoCuidadorRequest( String correo1, Response.Listener<String> listener){


        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params= new HashMap<>();
        Log.e("peticion",correo1);
        params.put("cc",correo1);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
