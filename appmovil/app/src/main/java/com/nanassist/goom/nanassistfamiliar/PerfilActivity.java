package com.nanassist.goom.nanassistfamiliar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.nanassist.goom.nanassistfamiliar.Model.Cliente;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesDentroDeCasa;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesFueraDeCasa;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;


public class PerfilActivity extends NavegacionActivity {


    private FirebaseAuth auth;
    private Uri photoUrl;
    private FirebaseFirestore db;
    public String nombre, correo, celular, direccion, visual, auditiva, fisica, cognitiva ,rutaimagen;
    ImageView visuali,auditivai,fisicai,cognitivai;
    private  AlertDialog.Builder mBuilder;
    private Dialog alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_perfil, null, false);
        drawer.addView(contentView, 0);
        visuali=findViewById(R.id.imageVisual);
        auditivai=findViewById(R.id.imageAuditiva);
        fisicai=findViewById(R.id.imageFisica);
        cognitivai=findViewById(R.id.imageCognitiva);
        auth =FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        mBuilder = new AlertDialog.Builder(PerfilActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.loading,null);
        mBuilder.setView(mView);
        alerta = mBuilder.show();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            Log.e("WIFI","TIENE CONEXIÓN");
            photoUrl =  auth.getCurrentUser().getPhotoUrl();
            if (photoUrl != null) {

                ImageView imageView = findViewById(R.id.perfilImagen);
                Picasso.get()
                        .load(photoUrl)
                        .transform(new CropCircleTransformation())
                        .into(imageView);

            }
        } else {
            if(leerArchivo()==false) {
                alerta.dismiss();
                AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(PerfilActivity.this);
                View mView2 = getLayoutInflater().inflate(R.layout.noconnection, null);
                mBuilder2.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Intent cambioPrincipal = new Intent(PerfilActivity.this, PrincipalActivity.class);
                        PerfilActivity.this.startActivity(cambioPrincipal);
                        dialog.cancel();

                    }
                });
                mBuilder2.setView(mView2);
                mBuilder2.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                    }
                }, 5000);
                return;
            }

        }


        if(leerArchivo()==false) {

            db = FirebaseFirestore.getInstance();
            DocumentReference docRef = db.collection("Clientes").document(auth.getCurrentUser().getUid());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {

                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Cliente cliente = document.toObject(Cliente.class);
                            correo = cliente.getEmail();
                            nombre = cliente.getName();
                            celular = cliente.getPhone();
                            direccion = cliente.getAddress();

                        } else {
                            Log.d("TAG", "No such document");
                        }
                    } else {
                        Log.d("TAG", "get failed with ", task.getException());
                    }
                }
            });


            //Carga datos discapacidades
            DocumentReference docRef2 = db.collection("DiscapacidadClientes").document(auth.getCurrentUser().getUid());
            docRef2.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {

                            visual = (String) document.get("visual");
                            auditiva = (String) document.get("auditiva");
                            Log.e("FOTOx",auditiva);
                            cognitiva = (String) document.get("cognitiva");
                            fisica = (String) document.get("fisica");
                            guardarArchivo();
                            cargarElementos();



                        } else {

                            Log.e("FOTO","NO SE ENCONTRÓ");
                        }
                    } else {
                        Log.e("FOTO", "get failed with ", task.getException());
                    }
                }
            });



        }
        else
        {
            cargarElementos();
        }



    }

    public void cargarElementos()
    {
        TextView txtCorreoPerfil = (TextView) findViewById(R.id.correoPerfil);
        txtCorreoPerfil.setText(correo);

        TextView txtNombre = (TextView) findViewById(R.id.tv_name);
        txtNombre.setText(nombre);

        TextView txtTelefono = (TextView) findViewById(R.id.telefonoPerfil);
        txtTelefono.setText(celular);

        TextView txtDireccion = (TextView) findViewById(R.id.direccionPerfil);
        txtDireccion.setText(direccion);

        tratarImagenes();
        alerta.dismiss();

    }


    public boolean leerArchivo()
    {
        SharedPreferences preferences = getSharedPreferences("datosperfil",MODE_PRIVATE);
        String ini = preferences.getString("nombre", "fg48p");
        if(ini.equals("fg48p"))
        {
            return false;
        }
        else
        {
            Log.e("TAGGGG","ENTRO A LEER 2");
            nombre = preferences.getString("nombre", "fg48p");
            correo = preferences.getString("correo", "error");
            celular = preferences.getString("celular","error");
            direccion  = preferences.getString("direccion", "error");
            visual = preferences.getString("visual","error");
            auditiva = preferences.getString("auditiva","error");
            cognitiva = preferences.getString("cognitiva","error");
            fisica = preferences.getString("fisica","error");
            rutaimagen = preferences.getString("rutaimg","error");
            Log.e("IMAGEN:",rutaimagen);
            obtenerImagenPerfil();

        }
        return true;
    }
    public void guardarArchivo()
    {
        SharedPreferences preferences = getSharedPreferences("datosperfil",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();


        editor.putString("nombre", nombre);
        Picasso.get().load(photoUrl).into(picassoImageTarget(getApplicationContext(), "NanAssist", "profile_img.JPEG"));
        if(nombre==null)
        {
            Log.e("Guardar", "Nombre nulo guardar");
        }

        editor.putString("correo",correo);
        editor.putString("celular",celular);
        editor.putString("direccion",direccion);
        editor.putString("direccion",direccion);
        editor.putString("visual",visual);
        editor.putString("auditiva", auditiva);
        editor.putString("cognitiva", cognitiva);
        editor.putString("fisica", fisica);
        editor.putString("rutaimg","profile_img.jpg");
        picassoImageTarget(this,"NanAssist","profile_img.jpg");
        editor.commit();
    }

    private void tratarImagenes()
    {
        if(cognitiva!=null)
        if(cognitiva.equals("0"))
            cognitivai.setAlpha(50);
        if(fisica!=null)
        if(fisica.equals("0"))
            fisicai.setAlpha(50);
        if(auditiva!=null)
        if(auditiva.equals("0"))
            auditivai.setAlpha(50);
        if(visual!=null)
        if(visual.equals("0"))
            visuali.setAlpha(50);

    }

    private void obtenerImagenPerfil()
    {
        ImageView imageView = findViewById(R.id.perfilImagen);
        //File myImageFile = new File(PicassoImageDownloader.getFileFullPath("ImageName"));
        Picasso.get()
                .load("/data/user/0/com.nanassist.goom.nanassistfamiliar/app_NanAssist/"+rutaimagen)
                .transform(new CropCircleTransformation())
                .into(imageView);
    }




    @Override
    public void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }



    private Target picassoImageTarget(Context context, final String imageDir, final String imageName) {
        Log.d("picassoImageTarget", " picassoImageTarget");
        ContextWrapper cw = new ContextWrapper(context);
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE); // path to /data/data/yourapp/app_imageDir
        return new Target() {
            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final File myImageFile = new File(directory, imageName); // Create image file
                        Log.e("picassoImageTarget", directory+imageName);
                        FileOutputStream fos = null;




                        try {
                            fos = new FileOutputStream(myImageFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                            fos.flush();
                            fos.close();

                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.i("image", "image saved to >>>" + myImageFile.getAbsolutePath());

                    }
                }).start();
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            public void onPrepareLoad(Drawable placeHolderDrawable) {
                if (placeHolderDrawable != null) {}
            }
        };
    }

}





