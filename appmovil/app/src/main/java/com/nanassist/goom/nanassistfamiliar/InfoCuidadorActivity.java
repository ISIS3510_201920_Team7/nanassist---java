package com.nanassist.goom.nanassistfamiliar;

import android.content.Intent;
import android.icu.text.IDNA;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class InfoCuidadorActivity extends AppCompatActivity {

    TextView nombre, tvMail, tvCel;
    RatingBar rb;

    String correofamiliar, correo,celular, calificacion, numcalificaciones, identificador;
    Button calificar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_cuidador);
        Intent i=getIntent();
        rb = (RatingBar) findViewById(R.id.ratingBar);

        identificador = i.getStringExtra("identificador");
        //Se vinculan las imagenes
        calificacion = i.getStringExtra("calificacion");
        rb.setRating(Float.valueOf(calificacion));
        rb.setEnabled(false);
        numcalificaciones = i.getStringExtra("numcalificaciones");
        nombre = findViewById(R.id.etDir1);
        nombre.setText(getIntent().getStringExtra("nombre"));
        calificar = findViewById(R.id.button);


        tvCel=findViewById(R.id.tvTelefono);
        tvMail=findViewById(R.id.tvMail);

        correofamiliar= i.getStringExtra("correofamiliar");
        correo=i.getStringExtra("correo");
        celular=i.getStringExtra("celular");
        tvMail.setText(correo);
        tvCel.setText(celular);


        calificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ir = new Intent(InfoCuidadorActivity.this, CalificarCuidador.class);
                ir.putExtra("calificacion", calificacion);
                ir.putExtra("numcalificaciones",numcalificaciones);
                ir.putExtra("correo",correo);
                ir.putExtra("correofamiliar",correofamiliar);
                ir.putExtra("identificador",identificador);
                InfoCuidadorActivity.this.startActivity(ir);

            }
        });

;

    }


}
