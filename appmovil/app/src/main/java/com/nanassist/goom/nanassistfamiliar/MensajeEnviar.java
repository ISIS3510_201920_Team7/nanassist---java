package com.nanassist.goom.nanassistfamiliar;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.ServerTimestamp;
import com.google.type.Date;


import java.util.Map;

public class MensajeEnviar extends Mensaje {
    //private Map hora;

    public MensajeEnviar() {
    }

    //public MensajeEnviar(Map hora) {
      //  this.hora = hora;
    //}

    public MensajeEnviar(String mensaje, String nombre, String fotoPerfil, String type_mensaje) {
        super(mensaje, nombre, fotoPerfil, type_mensaje);
        //this.hora = hora;
    }

    public MensajeEnviar(String mensaje, String urlFoto, String nombre, String fotoPerfil, String type_mensaje) {
        super(mensaje, urlFoto, nombre, fotoPerfil, type_mensaje);
        //this.hora = hora;
    }

    /*
    public Map getHora() {
        return hora;
    }

    public void setHora(Map hora) {
        this.hora = hora;
    }

     */
}