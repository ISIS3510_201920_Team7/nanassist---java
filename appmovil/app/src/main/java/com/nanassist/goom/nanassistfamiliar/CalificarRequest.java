package com.nanassist.goom.nanassistfamiliar;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.nanassist.goom.nanassistfamiliar.ipconstante;

import java.util.HashMap;
import java.util.Map;

public class CalificarRequest extends StringRequest{
    private static final String REGISTER_REQUEST_URL = "http://"+ ipconstante.ip+"/Calificacion.php";
    private Map<String, String> params;

    public CalificarRequest( String correo, String correofamiliar, float calificacion, Response.Listener<String> listener){


        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params= new HashMap<>();
        params.put("correofamiliar", correofamiliar);
        params.put("correocuidador", correo);
        params.put("calificacion", calificacion + "");

    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
