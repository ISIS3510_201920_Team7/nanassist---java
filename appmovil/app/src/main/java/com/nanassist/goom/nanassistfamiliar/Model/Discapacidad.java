package com.nanassist.goom.nanassistfamiliar.Model;

public class Discapacidad {
    private String email, visual,auditiva,fisica,cognitiva;

    public Discapacidad() {
    }

    public Discapacidad(String email, String visual, String auditiva, String fisica, String cognitiva) {
        this.email = email;
        this.visual = visual;
        this.auditiva = auditiva;
        this.fisica = fisica;
        this.cognitiva = cognitiva;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVisual() {
        return visual;
    }

    public void setVisual(String visual) {
        this.visual = visual;
    }

    public String getAuditiva() {
        return auditiva;
    }

    public void setAuditiva(String auditiva) {
        this.auditiva = auditiva;
    }

    public String getFisica() {
        return fisica;
    }

    public void setFisica(String fisica) {
        this.fisica = fisica;
    }

    public String getCognitiva() {
        return cognitiva;
    }

    public void setCognitiva(String cognitiva) {
        this.cognitiva = cognitiva;
    }
}
