package com.nanassist.goom.nanassistfamiliar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nanassist.goom.nanassistfamiliar.Model.Cliente;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class Register2Activity extends AppCompatActivity {

    CheckBox cbVisual,cbAuditiva,cbFisica, cbCognitiva;

    int discapacidad =0;
    String nombre;
    String correo;
    String contrasenia;
    String celular;
    String direccion;
    String sDiscapacidad;
    String visual, auditiva, fisica, cognitiva;

    private FirebaseAuth auth;
    private FirebaseFirestore db;
    CollectionReference users;


    private FirebaseStorage storage;
    private StorageReference rStorage;

    RelativeLayout registerLayout;

    // Imagen
    private static final int SELECT_FILE = 1;
    Uri imageUri;
    ImageView foto_gallery;
    private String uid = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_familiar);
        final Button bConfirmar = (Button) findViewById(R.id.btConfirmar);
        final Button bCancelar = (Button) findViewById(R.id.btCancelar);
        cbVisual = (CheckBox) findViewById(R.id.cb1);
        cbAuditiva = (CheckBox) findViewById(R.id.cb2);
        cbFisica = (CheckBox) findViewById(R.id.cb3);
        cbCognitiva= (CheckBox) findViewById(R.id.cb4);
        //----------------------------------
        Intent iRegistro = getIntent();
        Bundle extras = iRegistro.getExtras();
        if(extras!=null)
        {
            nombre = extras.getString("nombre");
            correo = extras.getString("correo");
            contrasenia = extras.getString("contrasenia");
            celular = extras.getString("celular");
            direccion = extras.getString("direccion");
        }
        //-----------------------------------


        db = FirebaseFirestore.getInstance();


        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        users = db.collection("Clientes");
        storage = FirebaseStorage.getInstance();
        rStorage = storage.getReference();
        registerLayout=(RelativeLayout) findViewById(R.id.registerLayout);
        //-----------------------------------

        //-----Imagen------
        foto_gallery = (ImageView) findViewById(R.id.foto_gallery);

        foto_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Click", "Entro en el onclicklistener");
                openGallery();
            }
        });


        bConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Manejo de conectividad
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    Log.e("WIFI","TIENE CONEXIÓN");
                } else {

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(Register2Activity.this);
                    View mView = getLayoutInflater().inflate(R.layout.noconnection, null);
                    mBuilder.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    mBuilder.setView(mView);
                    mBuilder.show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                        }
                    }, 5000);
                    return;

                }


                sDiscapacidad = ""+discapacidad;

                visual="0";
                auditiva="0";
                fisica="0";
                cognitiva="0";

                if(cbVisual.isChecked())
                {visual="1";}
                if(cbAuditiva.isChecked())
                {auditiva="1";}
                if(cbFisica.isChecked())
                {fisica="1";}
                if(cbCognitiva.isChecked())
                {cognitiva="1";}

                auth.createUserWithEmailAndPassword(correo,contrasenia).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Cliente cliente = new Cliente();
                        cliente.setName(nombre);
                        cliente.setPassword(contrasenia);
                        cliente.setEmail(correo);
                        cliente.setPhone(celular);
                        cliente.setAddress(direccion);
                        cliente.setDiscapacidad(sDiscapacidad);

                        logIn(correo, contrasenia);
                        String uid = auth.getCurrentUser().getUid();
                        //Toast toast1 =
                                Toast.makeText(getApplicationContext(),
                                        uid, Toast.LENGTH_LONG);

                      //  toast1.show();
                        Log.d("UID",uid);
                        users.document(uid).set(cliente);
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(nombre)
                                .build();

                        user.updateProfile(profileUpdates)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d("", "User profile updated.");
                                        }
                                    }
                                });

                        if(imageUri!=null){
                            StorageMetadata metadata;
                            UploadTask uploadTask;

                            metadata = new StorageMetadata.Builder()
                                    .setContentType("image/jpeg")
                                    .build();

                            uploadTask = rStorage.child("imagenes/perfil/"+auth.getCurrentUser().getUid()+"Prueba.jpg").putFile(imageUri, metadata);

                            // Listen for state changes, errors, and completion of the upload.
                            uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                                    System.out.println("Upload is " + progress + "% done");


                                }
                            }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                                    System.out.println("Upload is paused");
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                    rStorage.child("imagenes/perfil/"+auth.getCurrentUser().getUid()+"Prueba.jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {

                                            FirebaseUser user = auth.getCurrentUser();

                                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                                    .setPhotoUri(uri)
                                                    .build();

                                            user.updateProfile(profileUpdates)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                Log.e("Perfil", "Foto actualizada");
                                                            }
                                                        }
                                                    });
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception exception) {

                                        }
                                    });

                                }
                            });

                        }

                        Snackbar.make(registerLayout, "Registro exitoso", Snackbar.LENGTH_LONG).show();
                        cargarDiscapacidades();
                        Intent intent = new Intent(Register2Activity.this, PrincipalActivity.class);
                        Register2Activity.this.startActivity(intent);
                        finish();


                    }
                });


            }
        });
        bCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                Intent register1Intent = new Intent(Register2Activity.this, RegisterActivity.class);
                register1Intent.putExtra("nombre", nombre);
                register1Intent.putExtra("contrasenia", contrasenia);
                register1Intent.putExtra("correo", correo);
                register1Intent.putExtra("celular", celular);
                register1Intent.putExtra("direccion", direccion);
                Register2Activity.this.startActivity(register1Intent);
            }
        });
    }

    public void logIn(String correo, String contrasenia)
    {

        auth.signInWithEmailAndPassword(correo, contrasenia)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {



                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Register2Activity.this);
                            builder.setMessage("¡Ups! \n Correo y/o contraseña incorrecta firebase").setPositiveButton("Reintentar", null).create().show();
                        }

                    }
                });
    }
    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();


        switch(view.getId()) {
            case R.id.rbsi:
                if (checked)
                    discapacidad=1;
                habilitarCheckList();
                break;
            case R.id.rbno:
                if (checked)
                    discapacidad=0;
                deshabilitarCheckList();
                break;

        }
    }
    public void habilitarCheckList()
    {
        cbVisual.setClickable(true);
        cbAuditiva.setClickable(true);
        cbCognitiva.setClickable(true);
        cbFisica.setClickable(true);
        cbVisual.setTextColor(Color.BLACK );
        cbAuditiva.setTextColor(Color.BLACK );
        cbCognitiva.setTextColor(Color.BLACK );
        cbFisica.setTextColor(Color.BLACK );
    }
    public void deshabilitarCheckList()
    {
        cbVisual.setChecked(false);
        cbAuditiva.setChecked(false);
        cbCognitiva.setChecked(false);
        cbFisica.setChecked(false);
        cbVisual.setTextColor(Color.GRAY );
        cbAuditiva.setTextColor(Color.GRAY );
        cbCognitiva.setTextColor(Color.GRAY );
        cbFisica.setTextColor(Color.GRAY );

    }

    public void cargarDiscapacidades()
    {
        CollectionReference  disc = db.collection("DiscapacidadClientes");

        Map<String,Object> d = new HashMap<>();
        d.put("visual",visual);
        d.put("auditiva",auditiva);
        d.put("fisica",fisica);
        d.put("cognitiva",cognitiva);
        String uidaux = auth.getCurrentUser().getUid();
        disc.document(uidaux).set(d);
        guardarArchivo();



    }
    public void guardarArchivo()
    {
        SharedPreferences preferences = getSharedPreferences("datosperfil",MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("nombre", nombre);
        editor.putString("correo",correo);
        editor.putString("celular",celular);
        editor.putString("dirección",direccion);
        editor.putString("visual",visual);
        editor.putString("auditiva", auditiva);
        editor.putString("cognitiva", cognitiva);
        editor.putString("fisica", fisica);
    }

    private void openGallery(){
        Log.e("Click", "Intenta abrir la galeria");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen"),SELECT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        Uri selectedImageUri = null;

        String filePath = null;
        switch (requestCode) {
            case SELECT_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    imageUri = data.getData();
                    String selectedPath=imageUri.getPath();
                    if (requestCode == SELECT_FILE) {

                        if (selectedPath != null) {
                            InputStream imageStream = null;
                            try {
                                imageStream = getContentResolver().openInputStream(imageUri);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }

                            Bitmap bmp = BitmapFactory.decodeStream(imageStream);
                            foto_gallery.setImageBitmap(bmp);

                        }
                    }
                }
                break;
        }
    }


}
