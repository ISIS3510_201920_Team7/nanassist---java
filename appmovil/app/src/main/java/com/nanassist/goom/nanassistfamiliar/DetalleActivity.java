package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;

public class DetalleActivity extends NavegacionActivity {

    RequestQueue queue;
    TextView etDir1,etDir2,etFecha,etHora,etCategoria,etDescripcion, etEstado;
    Button bCancelar;
    String nombre,correo,correocuidador,correofamiliar,identificador;

    ImageView infoCuidador, localizacion;
    private FirebaseFirestore db;
    private FirebaseAuth auth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_detalle, null, false);
        drawer.addView(contentView, 0);
        auth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        bCancelar = findViewById(R.id.btnCancelar);
        String aux="o";
        if(getIntent().getStringExtra("asistente")!=null)
        {
        aux= getIntent().getStringExtra("asistente");
        }
        bCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eliminarRegistro();
            }
        });


        correocuidador = getIntent().getStringExtra("correocuidador");
        correofamiliar = getIntent().getStringExtra("correofamiliar");
        final String estado = getIntent().getStringExtra("estado");
        queue = Volley.newRequestQueue(DetalleActivity.this);
        etDir1=findViewById(R.id.etDir1);
        etDir1.setText(getIntent().getStringExtra("dir1"));
        etDir2=findViewById(R.id.etDir2);

        if(getIntent().getStringExtra("dir2")==null)
        {
            etDir2.setVisibility(View.GONE);
            findViewById(R.id.textView4).setVisibility(View.GONE);
            TextView ed3= (TextView)findViewById(R.id.textView3);
            ed3.setText("Dirección");
        }
        else {
            etDir2.setText(getIntent().getStringExtra("dir2"));
        }
        etFecha=findViewById(R.id.etFecha);
        etFecha.setText(getIntent().getStringExtra("fecha"));

        etHora=findViewById(R.id.etHora);
        etHora.setText(getIntent().getStringExtra("hora"));
        etCategoria=findViewById(R.id.etCategoria);
        etCategoria.setText(getIntent().getStringExtra("cat"));

        etDescripcion=findViewById(R.id.etDescripcion);
        etDescripcion.setText(getIntent().getStringExtra("des"));

        etEstado = findViewById(R.id.etEstado);
        etEstado.setText(estado);

        localizacion = findViewById(R.id.imageView4);
        infoCuidador=findViewById(R.id.imageView);
        infoCuidador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                perfilCuidador();
            }
        });

        if(estado.equals("disponible"))
        {
            infoCuidador.setEnabled(false);
            infoCuidador.setAlpha(35);

            localizacion.setEnabled(false);
            localizacion.setAlpha(35);
        }
        localizacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String aux =getIntent().getStringExtra("asistente");



                DocumentReference docRef = db.collection("localizacion").document(aux);
                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                Log.d("TAG", "DocumentSnapshot data: " + document.getData() + "longitud:"+document.getData().get("longitud"));
                                Intent i = new Intent(DetalleActivity.this, MapsActivity.class);
                                i.putExtra("longitud",document.getData().get("longitud").toString());
                                i.putExtra("latitud",document.getData().get("latitud").toString());
                                i.putExtra("asistente",aux);
                                startActivity(i);
                            } else {
                                Log.d("TAG", "No such document");
                            }
                        } else {
                            Log.d("TAG", "get failed with ", task.getException());
                        }
                    }
                });



            }
        });
    }

    private void eliminarRegistro(){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

            if (getIntent().getStringExtra("dir2") == null) {
                //String identificador = getIntent().getStringExtra("identificador");
                Log.e("identificador--", identificador);
                db.collection("PeticionesDentroDeCasa").document(identificador)
                        .delete();

                Log.d("ELIMINACION", "DocumentSnapshot successfully deleted!");
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetalleActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.delete, null);
                mBuilder.setView(mView);
                mBuilder.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        Intent intent = new Intent(DetalleActivity.this, PrincipalActivity.class);
                        DetalleActivity.this.startActivity(intent);
                        finish();
                    }
                }, 1500);

            }


            if (getIntent().getStringExtra("dir2") != null) {
                db.collection("PeticionesFueraDeCasa").document(identificador)
                        .delete();

                Log.d("ELIMINACION", "DocumentSnapshot successfully deleted!");
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetalleActivity.this);
                View mView = getLayoutInflater().inflate(R.layout.delete, null);
                mBuilder.setView(mView);
                mBuilder.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        Intent intent = new Intent(DetalleActivity.this, PrincipalActivity.class);
                        DetalleActivity.this.startActivity(intent);
                        finish();
                    }
                }, 1500);


            }
        }



         else {

            AlertDialog.Builder mBuilder2 = new AlertDialog.Builder(DetalleActivity.this);
            View mView2 = getLayoutInflater().inflate(R.layout.noconnection2, null);
            mBuilder2.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if(getIntent().getStringExtra("dir2")==null)
                    {
                        //String identificador = getIntent().getStringExtra("identificador");
                        Log.e("identificador--",identificador);
                        db.collection("PeticionesDentroDeCasa").document(identificador)
                                .delete();

                                        Log.d("ELIMINACION", "DocumentSnapshot successfully deleted!");
                                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetalleActivity.this);
                                        View mView = getLayoutInflater().inflate(R.layout.delete, null);
                                        mBuilder.setView(mView);
                                        mBuilder.show();
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {

                                                Intent intent = new Intent(DetalleActivity.this, PrincipalActivity.class);
                                                DetalleActivity.this.startActivity(intent);
                                                finish();
                                            }
                                        }, 1500);



                    }
                    if(getIntent().getStringExtra("dir2")!=null)
                    {
                        //String identificador = getIntent().getStringExtra("identificador");
                        Log.e("identificador--",identificador);
                        db.collection("PeticionesFueraDeCasa").document(identificador)
                                .delete();

                        Log.d("ELIMINACION", "DocumentSnapshot successfully deleted!");
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetalleActivity.this);
                        View mView = getLayoutInflater().inflate(R.layout.delete, null);
                        mBuilder.setView(mView);
                        mBuilder.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                Intent intent = new Intent(DetalleActivity.this, PrincipalActivity.class);
                                DetalleActivity.this.startActivity(intent);
                                finish();
                            }
                        }, 1500);

                    }
                }




            });
            mBuilder2.setView(mView2);
            mBuilder2.show();

    }

}
    private void perfilCuidador()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        String aux ="" ;
        if(getIntent().getStringExtra("asistente")!=null)
        {
            aux=getIntent().getStringExtra("asistente");
            Log.e("CAMPO", aux);
        }
        final String aux2 = aux;
        Log.e("CAMPO", aux);
        if (networkInfo != null && networkInfo.isConnected()) {
            Log.e("WIFI","TIENE CONEXIÓN");


            DocumentReference docRef = db.collection("Asistentes").document(aux);
            docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot document) {

                    Intent i = new Intent(DetalleActivity.this, InfoCuidadorActivity.class);
                    i.putExtra("nombre", document.get("Nombre").toString());
                    i.putExtra("correo", document.get("Correo").toString());
                    i.putExtra("celular", document.get("Celular").toString());

                    //alerta.dismiss();


                    //DetalleActivity.this.startActivity(i);
                    infoCalificacion(aux2,i);


                }
            });

        } else {

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetalleActivity.this);
            View mView = getLayoutInflater().inflate(R.layout.noconnection, null);
            mBuilder.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    //dialog.dismiss();
                }
            });
            mBuilder.setView(mView);
            mBuilder.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                }
            }, 5000);
            return;

        }
    }
    public void infoCalificacion(String identificador, Intent i)
    {
        final Intent intent = i;
        final String identificadorAux = identificador;
        DocumentReference docRef = db.collection("Calificaciones").document(identificador);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot document) {

                intent.putExtra("calificacion", document.get("calificacion").toString());
                intent.putExtra("numcalificaciones",document.get("numcalificaciones").toString());
                intent.putExtra("identificador",identificadorAux);
                DetalleActivity.this.startActivity(intent);


            }
        });
    }

}