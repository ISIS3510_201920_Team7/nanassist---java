package com.nanassist.goom.nanassistfamiliar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;


import java.util.ArrayList;
import java.util.List;

public class ActividadesCasaActivity extends  NavegacionActivity {
    GridLayout mainGrid;
    List<CardView> cards;
    String direccion,correo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_actividades_casa, null, false);
        drawer.addView(contentView, 0);
        mainGrid=(GridLayout)findViewById(R.id.mainGrid);
        direccion = getIntent().getStringExtra("direccion");
        correo = getIntent().getStringExtra("correo");
        cards = new ArrayList<>();
        agregarCard();

    }

    private void agregarCard(){
        //Row 1
        CardView cvAseo =findViewById(R.id.cardAseo);
        cvAseo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Aseo");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvAseo);
        CardView cvOrganizar =findViewById(R.id.cardOrganizar);
        cvOrganizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Organizar");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvOrganizar);


        //Row 2
        CardView cvLavadoCocina =findViewById(R.id.cardLavadoCocina);
        cvLavadoCocina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Lavar cocina");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvLavadoCocina);
        CardView cvLavarRopa =findViewById(R.id.cardLavarRopa);
        cvLavarRopa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Lavar ropa");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvLavarRopa);

        //Row 3
        CardView cvPlanchar =findViewById(R.id.cardPlanchar);
        cvPlanchar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Planchar");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvPlanchar);
        CardView cvCocinar =findViewById(R.id.cardCocinar);
        cvCocinar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Cocinar");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvCocinar);

        //Row 4

        CardView cvEntretenimiento =findViewById(R.id.cardEntretenimiento);
        cvEntretenimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Entretenimiento");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvEntretenimiento);
        CardView cvTecnologia =findViewById(R.id.cardTecnologia);
        cvTecnologia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CategoriasIntent = new Intent(ActividadesCasaActivity.this, CasaActivity.class);
                CategoriasIntent.putExtra("correo",correo);
                CategoriasIntent.putExtra("direccion",direccion);
                CategoriasIntent.putExtra("categoriaC","Tecnología");
                ActividadesCasaActivity.this.startActivity(CategoriasIntent);

            }
        });
        cards.add(cvTecnologia);

    }
}
