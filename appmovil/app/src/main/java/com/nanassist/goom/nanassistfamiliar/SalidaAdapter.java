package com.nanassist.goom.nanassistfamiliar;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.nanassist.goom.nanassistfamiliar.Model.PeticionesFueraDeCasa;

public class SalidaAdapter extends FirestoreRecyclerAdapter<PeticionesFueraDeCasa, SalidaAdapter.SalidaHolder> {

    private OnItemClickListener listener;

    public SalidaAdapter(@NonNull FirestoreRecyclerOptions<PeticionesFueraDeCasa> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull SalidaHolder holder, int position, @NonNull PeticionesFueraDeCasa model) {
        holder.textViewTitle.setText(model.getCategoria());
        holder.textViewDescription.setText(model.getFecha());
        holder.textViewPriority.setText(model.getEstado());
    }

    @NonNull
    @Override
    public SalidaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.salida_item,
                parent, false);
        return new SalidaHolder(v);
    }

    class SalidaHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewDescription;
        TextView textViewPriority;

        public SalidaHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            textViewPriority = itemView.findViewById(R.id.text_view_priority);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getSnapshots().getSnapshot(position), position);
                    }

                }
            });
        }
    }
    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}