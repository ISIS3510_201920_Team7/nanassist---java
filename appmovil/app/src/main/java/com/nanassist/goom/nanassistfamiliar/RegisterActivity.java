package com.nanassist.goom.nanassistfamiliar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final Button bCancel= findViewById(R.id.bCancelar);
        final Button bRegister = findViewById(R.id.bRegister);
        final EditText etName= findViewById(R.id.etName);
        final EditText etPassword =  findViewById(R.id.etPassword);
        final EditText etCorreo= findViewById(R.id.etCorreo);
        final EditText etCelular=findViewById(R.id.etCelular);
        final EditText etDireccion=  findViewById(R.id.etDireccion);

//----------------------------------
        Intent iRegistro = getIntent();
        Bundle extras = iRegistro.getExtras();
        if(extras!=null)
        {
            etName.setText(extras.getString("nombre"));
            etCorreo.setText(extras.getString("correo"));
            etPassword.setText(extras.getString("contrasenia"));
            etCelular.setText(extras.getString("celular"));
            etDireccion.setText(extras.getString("direccion"));
        }
        //-----------------------------------



        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cancelIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                RegisterActivity.this.startActivity(cancelIntent);
            }
        });

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 String nombre = etName.getText().toString();
                 String contrasenia = etPassword.getText().toString();
                 String correo = etCorreo.getText().toString();
                 String celular = etCelular.getText().toString();
                 String direccion = etDireccion.getText().toString();


                //Validacion de campos
                if(TextUtils.isEmpty(nombre)||TextUtils.isEmpty(contrasenia)||TextUtils.isEmpty(correo)||TextUtils.isEmpty(correo)||TextUtils.isEmpty(celular)
                        ||TextUtils.isEmpty(direccion)){
                if(TextUtils.isEmpty(nombre)) {
                    etName.setError("Campo obligatorio");
                }
                if(TextUtils.isEmpty(contrasenia)) {
                    etPassword.setError("Campo obligatorio");
                }
                if(TextUtils.isEmpty(correo)) {
                    etCorreo.setError("Campo obligatorio");
                }
                if(TextUtils.isEmpty(correo))
                {
                    etCorreo.setError("Campo obligatorio");
                }
                if(!TextUtils.isEmpty(correo)&&(!correo.contains("@")||!correo.contains(".com")))
                {
                    etCorreo.setError("Correo invalido");
                }

                if(TextUtils.isEmpty(celular)) {
                    etCelular.setError("Campo obligatorio");
                }
                if(TextUtils.isEmpty(direccion)) {
                    etDireccion.setError("Campo obligatorio");
                }

                }
                else if(!correo.contains("@")||!correo.contains(".com"))
                {
                    etCorreo.setError("Correo invalido");
                }


                else {

                    Intent register1Intent = new Intent(RegisterActivity.this, Register2Activity.class);
                    register1Intent.putExtra("nombre", nombre);
                    register1Intent.putExtra("contrasenia", contrasenia);
                    register1Intent.putExtra("correo", correo);
                    register1Intent.putExtra("celular", celular);
                    register1Intent.putExtra("direccion", direccion);


                    RegisterActivity.this.startActivity(register1Intent);
                }


            }
        });
    }
}