package com.nanassist.goom.nanassistfamiliar.Model;

import java.io.Serializable;

public class PeticionesFueraDeCasa implements Serializable {

    String categoria, correo, descripcion, direccion1,direccion2, fecha, hora, usuario,estado, identificador, asistente;


    public PeticionesFueraDeCasa ()
    {

    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setDireccionOrigen(String direccion) {
        this.direccion1 = direccion;
    }

    public void setDireccionDestino(String direccion) {
        this.direccion2 = direccion;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getCorreo() {
        return correo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDireccion1() {
        return direccion1;
    }
    public String getDireccion2() { return direccion2; }

    public String getFecha() {
        return fecha;
    }

    public String getHora() {
        return hora;
    }

    public String getUsuario() {
        return usuario;
    }
    public String getEstado(){return estado;}


    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getIdentificador() {
        return identificador;
    }


    public void setAsistente(String idAsistente) {
        this.asistente = idAsistente;
    }

    public String getAsistente() {
        return asistente;
    }
}
