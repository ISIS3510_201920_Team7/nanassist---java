package com.nanassist.goom.nanassistfamiliar;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String latitud="",longitud="";
    FirebaseFirestore db;
    Marker marcador;
    String idAsistente;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        db = FirebaseFirestore.getInstance();

        latitud=getIntent().getStringExtra("latitud");
        longitud=getIntent().getStringExtra("longitud");
        idAsistente =getIntent().getStringExtra("asistente");
        Log.e("ma","l: "+latitud+" "+longitud);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        double la=Double.valueOf(latitud);
        double lo=Double.valueOf(longitud);
        LatLng position = new LatLng(la,lo);
        marcador= mMap.addMarker(new MarkerOptions().position(position).title("Asistente"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,15));

        final DocumentReference docRef = db.collection("localizacion").document(idAsistente);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("", "Current data: " + snapshot.getData());
                    LatLng position = new LatLng(Double.valueOf(snapshot.getData().get("latitud").toString()),Double.valueOf(snapshot.getData().get("longitud").toString()));
                    marcador.remove();
                    marcador= mMap.addMarker(new MarkerOptions().position(position).title("Asistente"));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,15));
                } else {
                    Log.d("", "Current data: null");
                }
            }
        });

    }
}
