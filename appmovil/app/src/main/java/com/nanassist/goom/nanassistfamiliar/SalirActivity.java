package com.nanassist.goom.nanassistfamiliar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;


public class SalirActivity extends AppCompatActivity {



    Spinner comboDireccion;
    Spinner comboDireccion2;
    public String dir1S = "";
    public String dir2S = "";
    public String direccion1 = "";
    public String direccion2 = "";
    public String fecha = "";
    public String shora = "";
    public String descripcion = "";
    public String correo = "";
    public String categoria = "";
    private int dia, mes, anio, hora, minutos;
    EditText etFecha, etHora;
    Button bAgendar, bCancelar;
    private FirebaseAuth auth;
    private FirebaseFirestore db;
    CollectionReference peticionesFueraDeCasa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salir);
        final EditText etDir1 = findViewById(R.id.etDirN1);
        final EditText etDir2 = findViewById(R.id.etDirN2);
        final EditText etDir3 = findViewById(R.id.etDirN3);
        final EditText et2Dir1 = findViewById(R.id.etDir2N1);
        final EditText et2Dir2 = findViewById(R.id.etDir2N2);
        final EditText et2Dir3 = findViewById(R.id.etDir2N3);
        final EditText etDescripcion = findViewById(R.id.etDescripcion);
        etFecha = findViewById(R.id.etFecha);
        etHora = findViewById(R.id.etHora);
        bAgendar = findViewById(R.id.bAgendarServicio);
        bCancelar = findViewById(R.id.bCancelar);
        db = FirebaseFirestore.getInstance();


        auth = FirebaseAuth.getInstance();
        correo = auth.getCurrentUser().getEmail();
        db = FirebaseFirestore.getInstance();
        peticionesFueraDeCasa = db.collection("Peticiones");

        comboDireccion = findViewById(R.id.dir);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.combo_direccion, android.R.layout.simple_spinner_item);
        comboDireccion.setAdapter(adapter);
        comboDireccion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dir1S = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        comboDireccion2 = findViewById(R.id.dir2);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.combo_direccion, android.R.layout.simple_spinner_item);
        comboDireccion2.setAdapter(adapter2);
        comboDireccion2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                dir2S = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //------------ Pantallas de fecha y hora -----------------------
        etFecha = findViewById(R.id.etFecha);
        etHora = findViewById(R.id.etHora);
        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date date = new Date();
                String fecha = dateFormat.format(date);
                final Calendar c = Calendar.getInstance();
                dia = c.get(Calendar.DAY_OF_MONTH);
                mes = c.get(Calendar.MONTH);
                anio = c.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SalirActivity.this, R.style.DialogFechaTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        if (monthOfYear < 10 && dayOfMonth < 10)
                            etFecha.setText("0" + dayOfMonth + "/" + "0" + monthOfYear + "/" + year);
                        else if (monthOfYear < 10) {
                            etFecha.setText(dayOfMonth + "/" + "0" + monthOfYear + "/" + year);
                        } else if (dayOfMonth < 10) {
                            etFecha.setText("0" + dayOfMonth + "/" + monthOfYear + "/" + year);
                        } else {
                            etFecha.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                        }
                    }
                }
                        , Integer.valueOf(fecha.split("-")[0]), Integer.valueOf(fecha.split("-")[1]) - 1, Integer.valueOf(fecha.split("-")[2]));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });

        etHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                hora = c.get(Calendar.HOUR_OF_DAY);
                minutos = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(SalirActivity.this, R.style.DialogFechaTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay < 10 && minute < 10) {
                            etHora.setText("0" + hourOfDay + ":" + "0" + minute);
                        } else if (hourOfDay < 10) {
                            etHora.setText("0" + hourOfDay + ":" + minute);
                        } else if (minute < 10) {
                            etHora.setText(hourOfDay + ":" + "0" + minute);
                        } else {
                            etHora.setText(hourOfDay + ":" + minute);
                        }
                    }
                }, hora, minutos, false);
                timePickerDialog.show();

            }
        });
        bAgendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                direccion1 = dir1S + " " + etDir1.getText().toString() + "#" + etDir2.getText().toString() + "-" + etDir3.getText().toString();
                direccion2 = dir2S + " " + et2Dir1.getText().toString() + "#" + et2Dir2.getText().toString() + "-" + et2Dir3.getText().toString();
                fecha = etFecha.getText().toString();
                shora = etHora.getText().toString();
                descripcion = etDescripcion.getText().toString();

                categoria = getIntent().getStringExtra("categoria");

                //Validacion de campos
                if (TextUtils.isEmpty(etDir1.getText().toString()) || TextUtils.isEmpty(etDir2.getText().toString())
                        || TextUtils.isEmpty(etDir3.getText().toString()) || TextUtils.isEmpty(et2Dir1.getText().toString())
                        || TextUtils.isEmpty(et2Dir2.getText().toString()) || TextUtils.isEmpty(et2Dir3.getText().toString())
                ) {
                    if (TextUtils.isEmpty(etDir1.getText().toString())) {
                        etDir1.setError("");
                    }
                    if (TextUtils.isEmpty(etDir2.getText().toString())) {
                        etDir2.setError("");
                    }
                    if (TextUtils.isEmpty(etDir3.getText().toString())) {
                        etDir3.setError("");
                    }
                    if (TextUtils.isEmpty(et2Dir1.getText().toString())) {
                        et2Dir1.setError("");
                    }

                    if (TextUtils.isEmpty(et2Dir2.getText().toString())) {
                        et2Dir2.setError("");
                    }
                    if (TextUtils.isEmpty(et2Dir3.getText().toString())) {
                        et2Dir3.setError("");
                    }


                } else {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

                    if (networkInfo != null && networkInfo.isConnected()) {
                        Log.e("WIFI","TIENE CONEXIÓN");
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SalirActivity.this);
                        View mView = getLayoutInflater().inflate(R.layout.success, null);
                        mBuilder.setView(mView);
                        mBuilder.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                guardarPeticion();
                                Intent intent = new Intent(SalirActivity.this, PrincipalActivity.class);
                                SalirActivity.this.startActivity(intent);
                                finish();
                            }
                        }, 1200);
                    } else {

                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(SalirActivity.this);
                        View mView = getLayoutInflater().inflate(R.layout.noconnection2, null);
                        mBuilder.setNeutralButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(SalirActivity.this);
                                View mView = getLayoutInflater().inflate(R.layout.success, null);
                                mBuilder.setView(mView);
                                mBuilder.show();
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        Intent intent = new Intent(SalirActivity.this, PrincipalActivity.class);
                                        SalirActivity.this.startActivity(intent);
                                        finish();
                                    }
                                }, 1200);

                            }
                        });
                        mBuilder.setView(mView);
                        mBuilder.show();
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                guardarPeticion();

                            }
                        }, 1);


                    }


                }


            }
        });
        bCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void guardarPeticion() {
        Map<String, Object> d = new HashMap<>();
        d.put("direccion1", direccion1);
        d.put("direccion2", direccion2);
        d.put("fecha", fecha);
        d.put("hora", shora);
        d.put("descripcion", descripcion);
        d.put("categoria", categoria);
        d.put("estado", "disponible");
        d.put("usuario", auth.getCurrentUser().getUid() + "");
        d.put("asistente","Sin asignar");
        d.put("correo",correo);
        peticionesFueraDeCasa.add(d);

    }
}
