package com.nanassist.goom.nanassistfamiliar;

import android.app.Activity;
import android.os.Handler;
import android.os.Bundle;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;

public class SplashActivity extends Activity{

    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        auth =FirebaseAuth.getInstance();

        new Handler().postDelayed(new Runnable() {
            @Override
            /*
            * Metodo que cambia de actividad dependiendo de si el usuario está loggeado o no
            */

            public void run() {

                Intent intent;
                if(auth.getCurrentUser()==null) {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    intent = new Intent(SplashActivity.this, PrincipalActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        },1200);
    }
}
