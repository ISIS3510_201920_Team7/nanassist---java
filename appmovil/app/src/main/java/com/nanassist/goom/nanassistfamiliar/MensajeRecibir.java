package com.nanassist.goom.nanassistfamiliar;

import com.google.firebase.Timestamp;

public class MensajeRecibir extends Mensaje {

    private Timestamp hora;

    public MensajeRecibir() {
    }


    public MensajeRecibir(String mensaje, String urlFoto, String nombre, String fotoPerfil, String type_mensaje, Timestamp hora) {
        super(mensaje, urlFoto, nombre, fotoPerfil, type_mensaje);
        this.hora = hora;
    }

    public Timestamp getHora() {
        return hora;
    }

    public void setHora(Timestamp hora) {
        this.hora = hora;
    }
}